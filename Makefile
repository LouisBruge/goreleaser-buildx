CONTAINER_IMAGE ?= registry.gitlab.com/louisbruge/goreleaser-buildx

release: ## release docker image
	docker build -t ${CONTAINER_IMAGE} .
	docker push ${CONTAINER_IMAGE}
