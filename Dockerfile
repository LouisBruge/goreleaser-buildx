FROM goreleaser/goreleaser:v0.170.0

RUN mkdir -p /usr/lib/docker/cli-plugins

RUN curl -sL https://github.com/docker/buildx/releases/download/v0.5.1/buildx-v0.5.1.linux-amd64 --output /usr/lib/docker/cli-plugins/docker-buildx

RUN chmod a+x /usr/lib/docker/cli-plugins/docker-buildx
